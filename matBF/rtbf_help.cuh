#ifndef RTBF_HELP_CUH_
#define RTBF_HELP_CUH_

#include <string>

#include "spdlog/fmt/ostr.h"

auto helpBmode() {
  return fmt::format(
      "The Bmode operator performs envelope detection, (incoherent) "
      "compounding, and compression.\n"
      "\nRequired fields are: \n"
      "  operator     Must be 'Bmode'.\n"
      "\nOptional fields are: \n"
      "  axis         Axis for incoherent compounding (omit field if none).\n"
      "  compression  Either 'log' or 'power'.\n"
      "  gamma        Power compression factor (y = x.^gamma).\n");
}

auto helpChannelSum() {
  return fmt::format(
      "The ChannelSum operator reduces along an axis (outermost by "
      "default).\n"
      "\nRequired fields are: \n"
      "  operator  Must be 'ChannelSum'.\n"
      "\nOptional fields are: \n"
      "  axis      Axis for summation (0-indexed, -1 by default).\n"
      "  nchout    Number of subapertures to output (1 by default).\n");
}

auto helpDecimate() {
  return fmt::format(
      "The Decimate operator downsamples signals along a given axis\n"
      "after applying an anti-aliasing lowpass filter.\n"
      "\nRequired fields are: \n"
      "  operator  Must be 'Decimate'.\n"
      "  dsf       Downsample factor (will output ceil(N/dsf) samples).\n"
      "\nOptional fields are: \n"
      "  axis      Axis to decimate (0-indexed, 0 by default).\n");
}

auto helpDemodulate() {
  return fmt::format(
      "The Demodulate operator downshifts a signal by a given frequency.\n"
      "This is equivalent to multiplying by the complex phasor \n"
      "exp(-2j*pi*fd*t),where fd is the demodulation frequency and t is\n"
      " the time associated with each sample.\n"
      "\nRequired fields are: \n"
      "  operator  Must be 'Demodulate'.\n"
      "  t         Time vector (any singleton dimensions broadcasted).\n"
      "  fd        Demodulation frequency.\n"
      "\nOptional fields are: \n"
      "  inplace   Flag for in-place computation.\n");
}

auto helpFocus() {
  return fmt::format(
      "The Focus operator applies focusing time delays and optional "
      "summation.\nFocus has many capabilities, including synthetic transmit "
      "aperture (STA).\nPlease review the following fields carefully.\n"
      "\nRequired fields are: \n"
      "  operator   Must be 'Focus'.\n"
      "  pxpos      Pixel (x,y,z) positions as a [3, m, n, ...] array.\n"
      "             Can handle arbitrary pixel dimensions (2D, 3D, ...).\n"
      "  elpos      Element (x,y,z) positions as a [3, nelems] array\n"
      "  time0      The \"time zero\" of the acquisition. This parameter will\n"
      "             likely be the most difficult part to get right.\n"
      "             - For virtual source STA, time0 is the time corresponding\n"
      "               to when the wave is at the virtual source.\n"
      "             - For plane wave STA, time0 is the time corresponding\n"
      "               to when the wave passes through the origin.\n"
      "             time0 should also compensate for the pulse length\n"
      "             (excitation convolved by TX and RX temporal response).\n"
      "             Please refer to the examples for proper usage.\n"
      "  txdat      Transmit data is specified as:\n"
      "             - a [3, nxmits] array (virtual point source). \n"
      "               The (x,y,z) positions of the focal point.\n"
      "             - a [2, nxmits] array (plane wave).\n"
      "               The (azim, elev) angles in spherical coordinates.\n"
      "  fs         Sampling frequency. \n"
      "  fc         Center frequency (set to zero if demodulated). \n"
      "  c0         Nominal sound speed to use for computing delays. \n"
      "  txbf_mode  Transmit beamforming mode. There are 3 options:\n"
      "      1. 'IDENTITY' - Apply (virtual) transmit delays, but do not sum.\n"
      "      2. 'SUM'      - Apply (virtual) transmit delays and sum.\n"
      "      3. 'BLKDIAG'  - Apply (virtual) transmit delays, but treat\n"
      "                      each column (i.e. last pixel dimension) as\n"
      "                      independent (no STA). \"Traditional\" mode.\n"
      "  rxbf_mode  Receive beamforming mode. There are 2 options:\n"
      "      1. 'IDENTITY' - Apply receive delays, but do not sum.\n"
      "      2. 'SUM'      - Apply receive delays and sum.\n"
      "\nOptional fields are: \n"
      "  txfnum     (Virtual) transmit f-number. For virtual source STA, \n"
      "             creates an \"acceptance cone\" mask with respect to \n"
      "             each pixel. For plane wave STA, selects which angles \n"
      "             contribute to each pixel. Ignored for 'BLKDIAG' mode.\n"
      "  rxfnum     Receive f-number for receive aperture growth. \n"
      "  fdemod     If fc==0 due to demodulation, provide the demodulation \n"
      "             frequency for correct phase compensation.\n");
}

auto helpHilbert() {
  return fmt::format(
      "The Hilbert operator takes a real-valued signal and returns the\n"
      "complex-valued analytic signal. Hilbert operates on the innermost\n"
      "dimension (axis=0).\n"
      "\nRequired fields are: \n"
      "  operator  Must be 'Hilbert'.\n");
}

auto helpRefocus() {
  return fmt::format(
      "Refocus treats the transmit sequence as an encoding of the multistatic\n"
      "dataset and decodes it. There are three ways to use Refocus.\n"
      "In order of priority:\n"
      "1. User-supplied decoder \n"
      "  decoder  Must be a [#tx elem, #tx, #freqs] frequency-domain\n"
      "           representation of the decoder.\n"
      "2. User-supplied encoder \n"
      "  encoder  Must be a [#tx elem, #tx, #freqs] frequency-domain\n"
      "           representation of the encoder. rtbf will internally\n"
      "           compute the decoder via Tikhonov-regularized inversion.\n"
      "3. User-supplied transmit sequence\n"
      "  txdel    The transmit delays from the original sequence.\n"
      "  txapo    The transmit apodizations from the original sequence.\n"
      "  fs       The sampling frequency.\n"
      "In all cases, the operator field must be 'Refocus'. If Refocus is\n"
      "used, any subsequent Focus object should be modified so that its\n"
      "txdat corresponds to the element positions (i.e. multistatic STA).\n");
}

auto helpScanConvert2D() {
  return fmt::format(
      "The ScanFormat2D performs simple scan conversion from polar to\n"
      "Cartesian coordinates. If the input data is in (r,theta) coordinates,\n"
      "the image is resampled into the pixel positions given by pxpos.\n"
      "\nRequired fields are: \n"
      "  operator  Must be 'ScanConvert2D'.\n"
      "  r         The depths corresponding to the input data.\n"
      "  theta     The azimuthal angles corresponding to the input data.\n"
      "  pxpos     The desired pixel positions to interpolate into.\n"
      "\nOptional fields are: \n"
      "  apex      The 'apex' of the acquisition, i.e. the z-offset of the\n"
      "            origins of the polar and cartesian coordinate systems.\n"
      "  mode      There are 3 scan conversion modes (defaults to 'SECTOR'):\n"
      "        'SECTOR'       Sector view with no apex (pointed top).\n"
      "        'CURVILINEAR'  The apex is independent of angle (curved top).\n"
      "        'PHASED'       The apex depends on the angle (flat top).\n");
}

auto helpSVDFilter() {
  return fmt::format(
      "The SVDFilter operator performs spatiotemporal clutter filtering.\n"
      "The input signal is vectorized into a 'Casorati matrix', followed by\n"
      "a singular value decomposition (A = U*S*V'). The singular values are\n"
      "weighted and the matrix is recomposed to produce a filtered image\n"
      "(Afilt = U*(S.*W)*V'). SVD filtering is performed using cusolverDn's\n"
      "'gesvdp' algorithm.\n"
      "\nRequired fields are: \n"
      "  operator  Must be 'SVDFilter'.\n"
      "  weights   The weights to apply to the singular values. Should be\n"
      "            as a vector with length >= min(npixels, nframes).\n"
      "\nOptional fields are: \n"
      "  inplace   Flag to compute SVD result in place (in GPU memory).\n "
      "            Defaults to false.\n");
}

auto helpVSXFormatter() {
  return fmt::format(
      "The VSXFormatter operator will take the raw Verasonics receive buffer\n"
      "and reformat it to be used with rtbf. VSXFormatter can choose to treat\n"
      "the buffer as RF data or IQ data with proper resampling.\n"
      "\nRequired fields are: \n"
      "  operator    Must be 'VSXFormatter'.\n"
      "  nsamps      Number of int16 samples per channel per Tx/Rx event.\n"
      "  nelems      Total number of transducer elements to reconstruct with.\n"
      "  nchans      Number of channels in data acquisition.\n"
      "  nframes     Number of frames of data.\n"
      "  sampleMode  'NS200BW', 'BS100BW', 'BS50BW', or 'HILBERT'. The\n"
      "              first three options treat the data as IQ. The last\n"
      "              treats the data as RF, and is only valid when using\n"
      "              'NS200BW' mode on the Verasonics machine.\n"
      "\nOptional fields are: \n"
      "  nframes     Number of frames of data. Defaults to 1.\n"
      "  npulses     Number of repeated pulses to sum together (e.g., for \n"
      "              pulse inversion harmonic imaging). Defaults to 1.\n"
      "  chmap       A map from channels to elements, specified for each\n"
      "              transmit. This option is needed when working with multi-\n"
      "              plexed transducers, for instance. Assume a 1:1 mapping\n"
      "              if not specified.\n"
      "  dsf         Additional downsampling factor. Use with care, only if\n"
      "              you know your data has small bandwidth.\n");
}

auto helpsettings() {
  return fmt::format(
      "The following settings are available.\n"
      "  gpuID    Set the device to be used for this computational graph.\n"
      "           Defaults to 0.\n"
      "  verbose  Flag to output graph Tensor dimensions.\n"
      "           Defaults to false.\n");
}

auto helpdefault() {
  return fmt::format(
      "rtbf_mex v" PROJECT_VERSION
      "\n"
      "To see more detailed output, try rtbf_mex help <option>.\n\n"
      "Valid options are:\n"
      "  Bmode          Envelope detection, compounding, compression\n"
      "  ChannelSum     Sum along an axis (optional partial sums)\n"
      "  Decimate       Downsample with an anti-aliasing filter\n"
      "  Demodulate     Apply a frequency shift to data\n"
      "  Focus          Apply focusing time delays and apodizations\n"
      "  FocusLUT       Focus with lookup tables for delay and apod.\n"
      "  Hilbert        Apply a Hilbert transform to get analytic RF\n"
      "  Refocus        Recover the multistatic dataset\n"
      "  ScanConvert2D  Simple polar->cartesian scan conversion\n"
      "  SVDFilter      SVD for spatiotemporal clutter filtering\n"
      "  VSXFormatter   Verasonics raw data formatter\n"
      "  settings       See a list of other settings\n");
}

auto getHelpMessage(std::string option) {
  // Dispatch to help messages for each option
  if (option.compare("Bmode") == 0) {
    return helpBmode();
  } else if (option.compare("ChannelSum") == 0) {
    return helpChannelSum();
  } else if (option.compare("Decimate") == 0) {
    return helpDecimate();
  } else if (option.compare("Demodulate") == 0) {
    return helpDemodulate();
  } else if (option.compare("Focus") == 0) {
    return helpFocus();
  } else if (option.compare("Hilbert") == 0) {
    return helpHilbert();
  } else if (option.compare("Refocus") == 0) {
    return helpRefocus();
  } else if (option.compare("ScanConvert2D") == 0) {
    return helpScanConvert2D();
  } else if (option.compare("SVDFilter") == 0) {
    return helpSVDFilter();
  } else if (option.compare("VSXFormatter") == 0) {
    return helpVSXFormatter();
  } else if (option.compare("settings") == 0) {
    return helpsettings();
  } else {
    return helpdefault();
  }
}
#endif
/**
 @file vsxBF/utils/VSXFormatter.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2019-03-13

Copyright 2019 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use ile except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "VSXFormatter.cuh"

namespace rtbf {

template <typename T_in, typename T_out>
VSXFormatter<T_in, T_out>::VSXFormatter(
    std::shared_ptr<Tensor<T_in>> input, int numSamples, int numTransmits,
    int numElements, int numAcqChannels, VSXSampleMode sampleMode,
    std::optional<int> decimationFactor, int numRepeatedPulses, int numFrames,
    const std::optional<TensorView<int>> &h_channelMapping, int deviceID,
    cudaStream_t cudaStream, std::string moniker, std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);
  this->logdebug("Calling constructor.");

  // Populate parameters
  nsamps = numSamples;
  nxmits = numTransmits;
  nelems = numElements;
  nchans = numAcqChannels;
  nframes = numFrames;
  mode = sampleMode;
  npreps = numRepeatedPulses;
  gpuID = deviceID;
  this->stream = cudaStream;

  if (gpuID < 0) this->logerror("CPU version not implemented yet.");

  size_t nsamps_tmp, nsamps_out;
  if (mode == VSXSampleMode::BS50BW) {
    nsamps_tmp = nsamps * 4;
    dsfactor = (decimationFactor ? *decimationFactor : 4);
    this->loginfo("Selecting BS50BW ({}x decimation).", dsfactor);
  } else if (mode == VSXSampleMode::BS100BW) {
    nsamps_tmp = nsamps * 2;
    dsfactor = (decimationFactor ? *decimationFactor : 2);
    this->loginfo("Selecting BS100BW ({}x decimation).", dsfactor);
  } else if (mode == VSXSampleMode::NS200BW) {
    nsamps_tmp = nsamps * 1;
    dsfactor = (decimationFactor ? *decimationFactor : 1);
    this->loginfo("Selecting NS200BW ({}x decimation).", dsfactor);
  } else {
    nsamps_tmp = nsamps * 1;
    dsfactor = (decimationFactor ? *decimationFactor : 1);
    this->loginfo("Selecting HILBERT ({}x decimation).", dsfactor);
  }
  nsamps_out = nsamps_tmp / dsfactor;
  dsfactor = nsamps_tmp / nsamps_out;
  this->logdebug("{} input RF samples --> {} output IQ samples.", nsamps,
                 nsamps_out);

  if (gpuID >= 0) {
    // Initialize data arrays on current GPU
    CCE(cudaSetDevice(gpuID));

    // Load input Tensor information
    std::vector<size_t> idims = {nsamps * npreps * nxmits, nchans, nframes};
    if (idims != input->getDimensions()) {
      this->logerror(
          "Expected input to have dimensions {}, but has dimensions {}.",
          vecString(idims), vecString(input->getDimensions()));
    }
    this->in = input;

    // Initialize a temporary Tensor for FFT computations (only need 1 frame)
    std::vector<size_t> tdims = {nsamps_tmp, nxmits, nelems};
    tmp = std::make_shared<Tensor<cuda::std::complex<float>>>(
        tdims, gpuID, this->label + "->tmp", this->logName);

    // Initialize output Tensor
    std::vector<size_t> odims = {nsamps_out, nxmits, nelems, nframes};
    this->out = std::make_shared<Tensor<T_out>>(
        odims, gpuID, this->label + "->out", this->logName);
    oframesize = nsamps_out * nxmits * nelems;

    // Store the channel mapping as an array on the GPU if it is used
    if (h_channelMapping) {
      this->logdebug("Using custom channel mapping.");
      // Ensure that all channels are valid
      auto chmap = (*h_channelMapping).data();
      for (int x = 0; x < nxmits; x++) {
        for (int c = 0; c < nchans; c++) {
          if (chmap[c + nchans * x] < 0 || chmap[c + nchans * x] >= nelems)
            this->template logerror<std::domain_error>(
                "Custom channel map has an invalid value {} for channel {} on "
                "transmit {}. The value should be 0-indexed, i.e. in the range "
                "[0, {}].",
                chmap[c + nchans * x], c, x, nelems - 1);
        }
      }
      chanmap = std::make_shared<Tensor<int>>(
          *h_channelMapping, gpuID, this->label + "->cmap", this->logName);
    } else {
      this->logdebug("No channel map provided. Using default.");
      chanmap = nullptr;
    }

    // The temporary data will be processed as FFT->BPF->IFFT
    CCE(cufftCreate(&fftplan));
    int x = tdims[0];                // Number of valid samples
    int b = prod(tdims) / tdims[0];  // Number of batches
    CCE(cufftPlanMany(&fftplan, 1, &x, &x, 1, x, &x, 1, x, CUFFT_C2C, b));
    CCE(cufftSetStream(fftplan, this->stream));
  }
  // Get strides of data based on the data ordering (hard coded for now)
  VSXDataOrder dataOrder = VSXDataOrder::PXF;
  setVSXDataOrder(dataOrder);
}
template <typename T_in, typename T_out>
VSXFormatter<T_in, T_out>::~VSXFormatter() {
  reset();
  this->logdebug("Calling destructor.");
}
template <typename T_in, typename T_out>
void VSXFormatter<T_in, T_out>::reset() {
  // VSXFormatter does not require any additional cleanup.
}

template <typename T_in, typename T_out>
void VSXFormatter<T_in, T_out>::setVSXDataOrder(VSXDataOrder order) {
  // Compute strides between consecutive pulses, transmits, and Doppler frames
  int ns = nsamps;   // Number of samples
  int np = npreps;   // Number of pulses to sum
  int nx = nxmits;   // Number of transmission locations
  int nf = nframes;  // Number of Doppler frames, i.e., ensemble length
  if (order == VSXDataOrder::PXF) {
    pstride = ns;
    xstride = ns * np;
    fstride = ns * np * nx;
  } else if (order == VSXDataOrder::PFX) {
    pstride = ns;
    fstride = ns * np;
    xstride = ns * np * nf;
  } else if (order == VSXDataOrder::XPF) {
    xstride = ns;
    pstride = ns * nx;
    fstride = ns * nx * np;
  } else if (order == VSXDataOrder::FPX) {
    fstride = ns;
    pstride = ns * nf;
    xstride = ns * nf * np;
  } else if (order == VSXDataOrder::XFP) {
    xstride = ns;
    fstride = ns * nx;
    pstride = ns * nx * nf;
  } else if (order == VSXDataOrder::FXP) {
    fstride = ns;
    xstride = ns * nf;
    pstride = ns * nf * nx;
  }
}

// This function copies the data from the host to the GPU and selects
// the proper CUDA kernel to execute
template <typename T_in, typename T_out>
void VSXFormatter<T_in, T_out>::formatRawVSXData() {
  // Get data pointers and memory pitch
  short *idata = this->in->data();   // Input
  T_out *odata = this->out->data();  // Output

  // Loop through frames
  cufftComplex *tdata = reinterpret_cast<cufftComplex *>(tmp->data());
  for (int f = 0; f < nframes; f++) {
    // Unwrap the raw data into the tmp Tensor
    tmp->resetToZerosAsync(this->stream);  // Set all values to zero
    // Current frame corresponds to idata incremented by f*fstride
    unwrapData(idata + f * fstride);
    // FFT the data
    CCE(cufftExecC2C(fftplan, tdata, tdata, CUFFT_FORWARD));
    // Bandpass filter the data according to the VSXSampleMode
    filterData();
    // IFFT the data back
    CCE(cufftExecC2C(fftplan, tdata, tdata, CUFFT_INVERSE));
    // Finally, downsample, cast, and store data in output Tensor
    downsampleData(odata + f * oframesize);
  }
}

template <typename T_in, typename T_out>
void VSXFormatter<T_in, T_out>::unwrapData(short *idata) {
  // Each thread will process two IQ samples (hence nsamps / 2 in grid.x)
  // To do so, we reinterpret the input to actually be a complex short
  auto *idata2 = reinterpret_cast<cuda::std::complex<short> *>(idata);
  int nsamps2 = nsamps / 2;    // N RF samples == N/2 IQ samples
  int pstride2 = pstride / 2;  // N RF samples == N/2 IQ samples
  int xstride2 = xstride / 2;  // N RF samples == N/2 IQ samples
  this->logdebug("{} RF samples --> {} IQ samples.", nsamps, nsamps2);

  // Create computation grid
  dim3 B(256, 1, 1);
  dim3 G((nsamps2 - 1) / B.x + 1, (nxmits - 1) / B.y + 1,
         (nchans - 1) / B.z + 1);

  // If a channel map is given, use it.
  // TODO: Needs testing
  int *cmap = chanmap != nullptr ? chanmap->data() : nullptr;
  cuda::std::complex<float> *tdata = tmp->data();
  int ipitch = this->in->getPitch() / 2;  // divide by 2 for short->short2
  int tpitch = tmp->getPitch();  // No divide by 2 because tmp is already float2
  this->logdebug("{} ipitch, {} tpitch.", ipitch, tpitch);

  // First, copy raw data into output array with proper transpositions
  if (mode == VSXSampleMode::BS50BW) {  // 1 sample per cycle
    kernels::VSXFormatter::unwrapIQ<1><<<G, B, 0, this->stream>>>(
        nsamps2, npreps, nxmits, nchans, nelems, pstride2, xstride2, idata2,
        ipitch, tdata, tpitch, cmap);
  } else if (mode == VSXSampleMode::BS100BW) {  // 2 samples per cycle
    kernels::VSXFormatter::unwrapIQ<2><<<G, B, 0, this->stream>>>(
        nsamps2, npreps, nxmits, nchans, nelems, pstride2, xstride2, idata2,
        ipitch, tdata, tpitch, cmap);
  } else if (mode == VSXSampleMode::NS200BW) {  // 4 samples per cycle
    kernels::VSXFormatter::unwrapIQ<4><<<G, B, 0, this->stream>>>(
        nsamps2, npreps, nxmits, nchans, nelems, pstride2, xstride2, idata2,
        ipitch, tdata, tpitch, cmap);
  } else if (mode == VSXSampleMode::HILBERT) {  // Hilbert Transform version
    kernels::VSXFormatter::unwrapIQ<-1><<<G, B, 0, this->stream>>>(
        nsamps2, npreps, nxmits, nchans, nelems, pstride2, xstride2, idata2,
        ipitch, tdata, tpitch, cmap);
  }
}
template <typename T_in, typename T_out>
void VSXFormatter<T_in, T_out>::filterData() {
  // Create computation grid
  int nsamps_tmp = tmp->getDimensions()[0];
  dim3 B(256, 1, 1);
  dim3 G((nsamps_tmp - 1) / B.x + 1, (nxmits - 1) / B.y + 1,
         (nelems - 1) / B.z + 1);

  cuda::std::complex<float> *tdata = tmp->data();
  int tpitch = tmp->getPitch();

  // First, copy raw data into output array with proper transpositions
  // kernels::bandpassFilter<mode>
  //     <<<G, B, 0, this->stream>>>(nsamps_tmp, nxmits, nelems, tdata, tpitch);
  if (mode == VSXSampleMode::BS50BW) {  // 1 sample per cycle
    kernels::VSXFormatter::bandpassFilter<1>
        <<<G, B, 0, this->stream>>>(nsamps_tmp, nxmits, nelems, tdata, tpitch);
  } else if (mode == VSXSampleMode::BS100BW) {  // 2 samples per cycle
    kernels::VSXFormatter::bandpassFilter<2>
        <<<G, B, 0, this->stream>>>(nsamps_tmp, nxmits, nelems, tdata, tpitch);
  } else if (mode == VSXSampleMode::NS200BW) {  // 4 samples per cycle
    kernels::VSXFormatter::bandpassFilter<4>
        <<<G, B, 0, this->stream>>>(nsamps_tmp, nxmits, nelems, tdata, tpitch);
  } else if (mode == VSXSampleMode::HILBERT) {  // Hilbert Transform version
    kernels::VSXFormatter::bandpassFilter<-1>
        <<<G, B, 0, this->stream>>>(nsamps_tmp, nxmits, nelems, tdata, tpitch);
  }
}
template <typename T_in, typename T_out>
void VSXFormatter<T_in, T_out>::downsampleData(T_out *odata) {
  int nsamps_out = this->out->getDimensions()[0];
  // Create computation grid
  dim3 B(256, 1, 1);
  dim3 G((nsamps_out - 1) / B.x + 1, (nxmits - 1) / B.y + 1,
         (nelems - 1) / B.z + 1);
  int tpitch = tmp->getPitch();
  int opitch = this->out->getPitch();
  cuda::std::complex<float> *tdata = tmp->data();

  // Downsample the redundant resampled data to reduce data size.
  // Note: Hilbert Transform version does not need downsampling.
  if (mode == VSXSampleMode::BS50BW) {  // 1 sample per cycle
    kernels::VSXFormatter::downsampleIQ<T_out><<<G, B, 0, this->stream>>>(
        nsamps_out, nxmits, nelems, tdata, tpitch, odata, opitch, dsfactor);
  } else if (mode == VSXSampleMode::BS100BW) {  // 2 samples per cycle
    kernels::VSXFormatter::downsampleIQ<T_out><<<G, B, 0, this->stream>>>(
        nsamps_out, nxmits, nelems, tdata, tpitch, odata, opitch, dsfactor);
  } else if (mode == VSXSampleMode::NS200BW) {  // 4 samples per cycle
    kernels::VSXFormatter::downsampleIQ<T_out><<<G, B, 0, this->stream>>>(
        nsamps_out, nxmits, nelems, tdata, tpitch, odata, opitch, dsfactor);
  } else if (mode == VSXSampleMode::HILBERT) {  // Hilbert Transform version
    this->logdebug(
        "nsamps_out={}, nxmits={}, nelems={}, tdata={}, tpitch={}, odata={}, "
        "opitch={}, dsfactor={}",
        nsamps_out, nxmits, nelems, fmt::ptr(tdata), tpitch, fmt::ptr(odata),
        opitch, dsfactor);
    kernels::VSXFormatter::downsampleIQ<<<G, B, 0, this->stream>>>(
        nsamps_out, nxmits, nelems, tdata, tpitch, odata, opitch, dsfactor);
  }
}

template <int spc>
__global__ void kernels::VSXFormatter::unwrapIQ(
    int nsamps, int npreps, int nxmits, int nchans, int nelems, int pstride,
    int xstride, cuda::std::complex<short> *idata, int ipitch,
    cuda::std::complex<float> *tdata, int tpitch, int *cmap) {
  // Determine information about the current thread
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int xmit = blockIdx.y * blockDim.y + threadIdx.y;
  int chan = blockIdx.z * blockDim.z + threadIdx.z;

  // Only execute kernel if valid
  if (samp < nsamps && xmit < nxmits && chan < nchans) {
    // Output element index; destination of current data
    int elem = cmap == nullptr ? chan : cmap[chan + nchans * xmit];
    // Advance input and output data pointers to current sample
    // For spc==4, idata[samp] --> (tdata[2*samp+0].x, tdata[2*samp+1].y)
    // For spc==2, idata[samp] --> (tdata[4*samp+0].x, tdata[4*samp+1].y)
    // For spc==1, idata[samp] --> (tdata[8*samp+0].x, tdata[8*samp+1].y)
    idata += xmit * xstride + chan * ipitch;
    tdata += tpitch * (xmit + nxmits * elem);
    // Each thread writes two output IQ samples based on two input samples
    float sumI = 0.f, sumQ = 0.f;
    // Loop through summed pulses (e.g. pulse inversion harmonic imaging)
    for (int p = 0; p < npreps; p++) {
      // Accumulate values
      if (spc == 4 && samp & 1) {  // NS200BW: Negate every 3rd and 4th sample
        sumI -= (float)idata[samp + p * pstride].real();  // Grab I component
        sumQ -= (float)idata[samp + p * pstride].imag();  // Grab Q component
      } else {
        sumI += (float)idata[samp + p * pstride].real();  // Grab I component
        sumQ += (float)idata[samp + p * pstride].imag();  // Grab Q component
      }
    }
    // Write the sums for sample pairs into tdata
    if (spc == -1) {  // VSXSampleMode::HILBERT
      // In ordinary RF sampling mode, treat data as I, I, I, I, I, ...
      tdata[samp * 2 + 0].real(sumI);  // I
      tdata[samp * 2 + 1].real(sumQ);  // I
    } else {
      // In direct IQ sampling mode, treat data as I, Q, I, Q, I, ...
      tdata[samp * 8 / spc + 0].real(sumI);  // I
      tdata[samp * 8 / spc + 1].imag(sumQ);  // Q
    }
  }
}

/** @brief Bandpass filtering for anti-aliasing the upsampled data.
Verasonics data is already demodulated. The upsampling procedure in unwrapIQ
results in an aliased spectrum. This kernel applies an anti-aliasing bandpass
filter whose band depends on the samples per cycle of the VSXSampleMode.
*/
template <int spc>
__global__ void kernels::VSXFormatter::bandpassFilter(
    int nsamps, int nxmits, int nelems, cuda::std::complex<float> *tdata,
    int tpitch) {
  // Determine information about the current thread
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int xmit = blockIdx.y * blockDim.y + threadIdx.y;
  int chan = blockIdx.z * blockDim.z + threadIdx.z;

  // Only execute kernel if valid
  if (samp < nsamps && xmit < nxmits && chan < nelems) {
    // // Each frequency will be weighted according to the sampling mode
    float weight;
    if (spc == -1) {  // Hilbert Transform mode
      // In Hilbert transform mode, we zero out the negative part of the
      // frequency spectrum (i.e. when normalized frequency > 0.5).
      if (nsamps & 1) {  // Hilbert Transform (odd nsamps)
        if (samp == 0) {
          weight = 1.f / nsamps;
        } else if (samp < (nsamps + 1) / 2) {
          weight = 2.f / nsamps;
        } else {
          weight = 0.f / nsamps;
        }
      } else {  // Hilbert Transform (even nsamps)
        if (samp == 0 || samp == nsamps / 2) {
          weight = 1.f / nsamps;
        } else if (samp < nsamps / 2) {
          weight = 2.f / nsamps;
        } else {
          weight = 0.f / nsamps;
        }
      }
    } else {  // Direct IQ sampling mode
      // In direct IQ sampling mode, we just need to bandpass filter around
      // normalized frequency 0.5. The bandwidth is determined by the samples
      // per cycle (spc) set by the VSXSampleMode.
      float f = 1.f * samp / nsamps;  // Current thread's normalized frequency
      float bw = 1.f * spc / 8;       // Total bandwidth of valid signal
      if (f < bw / 2 || f >= 1.f - bw / 2) {
        weight = 1.f / bw / nsamps;
      } else {
        weight = 0.f;
      }
    }
    // Apply weights to current sample
    int idx = samp + tpitch * (xmit + nxmits * chan);
    tdata[idx] *= weight;
  }
}

/** @brief The final data is highly oversampled. Now that an anti-aliasing
 * bandpass filter has been applied, we can safely downsample the data without
 * loss of information.
 */
template <typename T_out>
__global__ void kernels::VSXFormatter::downsampleIQ(
    int nsamps_out, int nxmits, int nelems, cuda::std::complex<float> *tdata,
    int tpitch, T_out *odata, int opitch, int dsf) {
  // Determine information about the current thread
  int samp = blockIdx.x * blockDim.x + threadIdx.x;
  int xmit = blockIdx.y * blockDim.y + threadIdx.y;
  int elem = blockIdx.z * blockDim.z + threadIdx.z;

  // Only execute kernel if valid
  if (samp < nsamps_out && xmit < nxmits && elem < nelems) {
    // Advance pointers to the current transmit and channel
    tdata += tpitch * (xmit + nxmits * elem);
    odata += opitch * (xmit + nxmits * elem);
    // Store the oversampled cuda::std::complex<float> tdata into the type
    // T_out odata
    odata[samp] = complex_cast<T_out>(tdata[samp * dsf]);
  }
}

// Explicitly instantiate template for cuda::std::complex<short>,
// cuda::std::complex<float> outputs
template class VSXFormatter<short, cuda::std::complex<short>>;
template class VSXFormatter<short, cuda::std::complex<float>>;
}  // namespace rtbf

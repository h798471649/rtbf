%% Setup
addpath ../matBF
nrows = 400;
ncols = 300;
nframes = 64;

for str = {'real', 'complex'}
    if strcmp(str{:}, 'real')
        % Real SVD
        A = randn(nrows,ncols,nframes,'single');  % real SVD
    else
        % Complex SVD
        A = single(randn(nrows,ncols,nframes) + 1i*randn(nrows,ncols,nframes))/sqrt(2);
    end
    weights = rand(nframes,1,'single');  % SVD filter weights
    nruns = 100;  % Number of timing runs

    fprintf('Running a %s-valued SVD filter of data with size [%d, %d, %d]\n', ...
        str{:}, nrows, ncols, nframes);

    %% MATLAB CPU implementation
    tic
    Avec = reshape(A,[],size(A,3));
    for i = 1:nruns
        [U,S,V] = svd(Avec, 0);
        Bvec = U * (S .* weights) * V';
    end
    B = reshape(Bvec, size(A));
    tcpu = toc;
    fprintf('MATLAB (cpu) time per run: %6f\t', tcpu / nruns);
    fprintf('(%6f fps)\n', nruns / tcpu);

    %% MATLAB GPU implementation
    Avec = gpuArray(reshape(A,[],size(A,3)));
    tic
    for i = 1:nruns
        [U,S,V] = svd(Avec, 0);
        Bvec = U * (S .* weights) * V';
    end
    B = gather(reshape(Bvec, size(A)));
    tcpu = toc;
    fprintf('MATLAB (gpu) time per run: %6f\t', tcpu / nruns);
    fprintf('(%6f fps)\n', nruns / tcpu);

    %% rtbf GPU implementation
    SVD.operator = 'SVDFilter';
    SVD.weights = weights;
    SVD.inplace = true;
    % Initialize graph
    C = rtbf_mex(A, SVD);
    % Execute in real-time loop
    tic;
    for i = 1:nruns
        C = rtbf_mex(A);
    end
    clear rtbf_mex
    tgpu = toc;
    fprintf('rtbf (gpu) time per run: %6f\t', tgpu / nruns);
    fprintf('(%6f fps)\n', nruns / tgpu);

    %% Check if errors are reasonable
    rmse = rms(B(:)) - rms(C(:));
    if rmse > 0.1
        error('RMS error is high (%f).\n', rmse);
    end
end
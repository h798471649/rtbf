/**
 @file gpuBF/InputLoader.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-05-02

Copyright 2022 Dongwoon Hyun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef INPUTLOADER_CUH_
#define INPUTLOADER_CUH_

#include "Operator.cuh"

namespace rtbf {

template <typename T_in, typename T_out>
class InputLoader : public Operator<Tensor<T_in>, Tensor<T_out>> {
 private:
  // InputLoader members
  int gpuID;                 ///< Device to use for execution
  std::vector<size_t> dims;  ///< Dimensions of input

 public:
  InputLoader(std::vector<size_t> dimensions, int deviceID = 0,
              cudaStream_t cudaStream = 0, std ::string moniker = "InputLoader",
              std::string loggerName = "");
  /// @brief Alternate constructor with unused shared_ptr as "input"
  InputLoader(std::shared_ptr<Tensor<T_in>> dummyInput,
              std::vector<size_t> dimensions, int deviceID = 0,
              cudaStream_t cudaStream = 0, std ::string moniker = "InputLoader",
              std::string loggerName = "")
      : InputLoader(dimensions, deviceID, cudaStream, moniker, loggerName) {}
  virtual ~InputLoader();

  /// @brief Function to load data from a raw pointer
  void load(const T_out *h_raw, int pitchInSamps = 0);
  /// @brief Function to load data from a Tensor
  void load(Tensor<T_out> *input);
};

// Add template type traits
template <typename>
struct is_InputLoader : std::false_type {};
template <typename T_in, typename T_out>
struct is_InputLoader<InputLoader<T_in, T_out>> : std::true_type {};
}  // namespace rtbf

#endif

/**
 @file gpuBF/ChannelSum.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-03-31

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "ChannelSum.cuh"

namespace rtbf {
template <typename T_in, typename T_out>
ChannelSum<T_in, T_out>::ChannelSum(std::shared_ptr<Tensor<T_in>> input,
                                    int axisToSum, int nOutputChannels,
                                    cudaStream_t cudaStream,
                                    std::string moniker,
                                    std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);
  this->logdebug("Initializing {}.", this->label);

  // Read inputs
  this->in = input;              // Share ownership of input Tensor
  axis = axisToSum;              // Axis to sum over
  nchans_out = nOutputChannels;  // Number of channels to output
  this->stream = cudaStream;     // Set asynchronous stream

  // Allow Python-style negative indexing
  auto idims = input->getDimensions();
  if (axis < 0) axis += idims.size();
  if (axis < 0 && axis >= idims.size())
    this->template logerror<std::out_of_range>(
        "Invalid axis {} to sum over. Must be in range [-{}, {})", axis,
        idims.size(), idims.size());

  int dsf = idims[axis] / nOutputChannels;
  if (nchans_out > idims[axis]) {
    this->loginfo(
        "Requesting more output channels ({}) than available (dims[{}]={}). "
        "Setting to maximum.",
        nchans_out, axis, idims[axis]);
    nchans_out = idims[axis];
  }

  D = std::make_shared<Decimate<T_in, T_out>>(
      input, dsf, axis, cudaStream, moniker + "->Decimate", loggerName);

  this->out = D->getOutput();
}

template <typename T_in, typename T_out>
ChannelSum<T_in, T_out>::~ChannelSum() {
  // Nothing to do in destructor. Everything will naturally fall out of scope.
  this->loginfo("Destroyed {}.", this->label);
}

template <typename T_in, typename T_out>
void ChannelSum<T_in, T_out>::sumChannels(bool normalize) {
  this->logdebug("Executing channel sum.");
  D->decimate(normalize);
}

// Explicit template specialization instantiation for faster compiling
template class ChannelSum<cuda::std::complex<short>, cuda::std::complex<short>>;
template class ChannelSum<cuda::std::complex<short>, cuda::std::complex<int>>;
template class ChannelSum<cuda::std::complex<short>, cuda::std::complex<float>>;
template class ChannelSum<cuda::std::complex<int>, cuda::std::complex<int>>;
template class ChannelSum<cuda::std::complex<int>, cuda::std::complex<float>>;
template class ChannelSum<cuda::std::complex<float>, cuda::std::complex<float>>;
template class ChannelSum<short, short>;
template class ChannelSum<short, int>;
template class ChannelSum<short, float>;
template class ChannelSum<int, int>;
template class ChannelSum<int, float>;
template class ChannelSum<float, float>;

}  // namespace rtbf

/**
 @file gpuBF/SVDFilter.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-08-12

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "SVDFilter.cuh"

namespace rtbf {

template <typename T_in, typename T_out>
SVDFilter<T_in, T_out>::SVDFilter(std::shared_ptr<Tensor<T_in>> input,
                                  const Tensor<float> *h_weights,
                                  bool computeInPlace, cudaStream_t cudaStream,
                                  std ::string moniker,
                                  std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);
  this->logdebug("Initializing {}.", this->label);

  // Read inputs
  this->in = input;                 // Share ownership of input Tensor
  this->stream = cudaStream;        // Set asynchronous stream
  gpuID = this->in->getDeviceID();  // Execute on input's device
  inplace = computeInPlace;

  // Must run on the GPU
  if (gpuID < 0)
    this->template logerror<NotImplemented>(
        "No CPU implementation of SVDFilter yet.");

  // Compute the number of pixels
  auto idims = this->in->getDimensions();  // Get dimensions of input
  npixels = this->in->getPitch();
  for (size_t i = 1; i < idims.size() - 1; i++) npixels *= idims[i];
  nframes = idims.back();

  // Get the rank of the economic SVD
  rank = npixels > nframes ? nframes : npixels;

  // Load singular value weights
  W = Tensor<float>(*h_weights, gpuID, this->label + "->W", this->logName);
  if (prod(W.getDimensions()) < rank)
    this->template logerror<std::runtime_error>(
        "The number of weights ({}) is less than the requested rank of the SVD "
        "({}).",
        prod(W.getDimensions()), rank);

  // Create Tensors for singular decomposition components
  std::vector<size_t> udims{npixels, rank};
  std::vector<size_t> sdims{rank};
  std::vector<size_t> vdims{nframes, rank};
  S = Tensor<float>(sdims, gpuID, this->label + "->S", this->logName);
  U = Tensor<T_in>(udims, gpuID, this->label + "->U", this->logName);
  V = Tensor<T_in>(vdims, gpuID, this->label + "->V", this->logName);

  // Prepare cusolver variables
  m = npixels;
  n = nframes;
  lda = m;
  ldu = m;
  ldv = n;
  d_A = this->in->data();
  d_S = S.data();
  d_U = U.data();
  d_V = V.data();
  d_W = W.data();

  // Initialize cuSolverDn and cuBLAS handles
  CCE(cusolverDnCreate(&handle));
  CCE(cusolverDnSetStream(handle, this->stream));
  CCE(cusolverDnCreateParams(&params));
  CCE(cublasCreate(&bhandle));                  // Initialize cublas handle
  CCE(cublasSetStream(bhandle, this->stream));  // Initialize cublas stream

  // Query the working space of the SVD
  CCE(cusolverDnXgesvdp_bufferSize(handle, params, jobz, econ, m, n, typeA, d_A,
                                   lda, typeS, d_S, typeU, d_U, ldu, typeV, d_V,
                                   ldv, computeType, &d_lwork, &h_lwork));

  // Allocate the working space as needed
  std::vector<size_t> dspace{d_lwork};
  dw = Tensor<T_in>(dspace, gpuID, this->label + "->d_work", this->logName);
  di = Tensor<int>(std::vector<size_t>{1}, gpuID, this->label + "d_info",
                   this->logName);
  d_work = dw.data();
  d_info = di.data();
  if (h_lwork > 0) {
    std::vector<size_t> hspace{h_lwork};
    hw = Tensor<T_in>(hspace, -1, this->label + "->d_work", this->logName);
    h_work = hw.data();
  } else {
    h_work = nullptr;
  }

  // If in-place, do not allocate output
  if (inplace)
    this->out = this->in;
  else {
    // Define data dimensions
    std::vector<size_t> odims = this->in->getDimensions();
    // Initialize output array and other private arrays
    this->out = std::make_shared<Tensor<T_out>>(
        odims, gpuID, this->label + "->out", this->logName);
  }
}

template <typename T_in, typename T_out>
SVDFilter<T_in, T_out>::~SVDFilter() {
  this->loginfo("Destroyed {}.", this->label);
}

template <typename T_in, typename T_out>
void SVDFilter<T_in, T_out>::filter() {
  // Set CUDA device
  CCE(cudaSetDevice(gpuID));

  // Compute SVD
  CCE(cusolverDnXgesvdp(handle, params, jobz, econ, m, n, typeA, d_A, lda,
                        typeS, d_S, typeU, d_U, ldu, typeV, d_V, ldv,
                        computeType, d_work, d_lwork, h_work, h_lwork, d_info,
                        &h_err_sigma));

  // Apply filter to singular value weights
  auto getG = [](int b, int n) { return (n - 1) / b + 1; };
  dim3 blk(16, 16);
  dim3 grd(getG(blk.x, n), getG(blk.y, rank));
  kernels::SVDFilter::applyWeights<<<grd, blk, 0, this->stream>>>(
      d_S, d_W, d_V, int(n), int(rank));

  // Recombine
  if constexpr (is_complex<T_in>()) {
    const cuComplex alpha = make_float2(1.f, 0.f);
    auto *A = reinterpret_cast<cuComplex *>(d_U);
    int lda = m;
    auto *B = reinterpret_cast<cuComplex *>(d_V);
    int ldb = n;
    const cuComplex beta = make_float2(0.f, 0.f);
    auto *C = reinterpret_cast<cuComplex *>(this->out->data());
    int ldc = m;
    CCE(cublasCgemm3m(bhandle, CUBLAS_OP_N, CUBLAS_OP_C, m, n, rank, &alpha, A,
                      lda, B, ldb, &beta, C, ldc));
  } else {
    const float alpha = 1.f;
    auto *A = d_U;
    int lda = m;
    auto *B = d_V;
    int ldb = n;
    const float beta = 0.f;
    auto *C = this->out->data();
    int ldc = m;
    CCE(cublasSgemm(bhandle, CUBLAS_OP_N, CUBLAS_OP_C, m, n, rank, &alpha, A,
                    lda, B, ldb, &beta, C, ldc));
  }
}

///////////////////////////////////////////////////////////////////////////
// kernels
///////////////////////////////////////////////////////////////////////////

template <typename T_in>
__global__ void kernels::SVDFilter::applyWeights(float *S, float *W, T_in *V,
                                                 int n, int k) {
  // Determine information about the current thread
  int x = blockIdx.x * blockDim.x + threadIdx.x;
  int y = blockIdx.y * blockDim.y + threadIdx.y;
  if (x < n && y < k) V[x + n * y] *= S[y] * W[y];
}
// TODO: Add support for other input, output data types.
template class SVDFilter<float, float>;
template class SVDFilter<cuda::std::complex<float>, cuda::std::complex<float>>;
}  // namespace rtbf
/**
 @file gpuBF/Demodulate.cu
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-08-05

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "Demodulate.cuh"

namespace rtbf {
template <typename T_in, typename T_out>
Demodulate<T_in, T_out>::Demodulate(std::shared_ptr<Tensor<T_in>> input,
                                    const Tensor<float> *timeTensor,
                                    float freqDemod, bool performInPlace,
                                    cudaStream_t cudaStream,
                                    std::string moniker,
                                    std::string loggerName) {
  this->label = moniker;
  this->setLogger(loggerName);
  this->logdebug("Initializing {}.", this->label);

  if constexpr (!is_complex<T_out>()) {
    this->logerror("The output of Demodulate must be complex-valued.");
  }

  // Read inputs
  this->in = input;                   // Share ownership of input Tensor
  this->stream = cudaStream;          // Set asynchronous stream
  gpuID = this->in->getDeviceID();    // Execute on input Tensor's device
  idims = this->in->getDimensions();  // Get dimensions of input
  times = Tensor<float>(*timeTensor, gpuID, "d_times", this->logName);
  tdims = times.getDimensions();  // Get dimensions of time array
  fdemod = freqDemod;

  if (idims.size() == 0)
    this->template logerror<std::invalid_argument>(
        "Empty input Tensor provided.");
  if (tdims.size() == 0)
    this->template logerror<std::invalid_argument>(
        "Empty time Tensor provided.");

  // If the time Tensor has fewer dimensions than the input Tensor, assume that
  // remaining dimensions are singleton.
  while (tdims.size() < idims.size()) tdims.push_back(1);
  // Time Tensor should not be empty or have more dimensions than the input
  if (prod(tdims) <= 1 || tdims.size() > idims.size())
    this->template logerror<std::invalid_argument>(
        "The provided time array has invalid dimensions {} for an input "
        "Tensor with dimensions {}.",
        vecString(tdims), vecString(idims));
  // Make sure that the time vector has valid size
  for (int i = 0; i < tdims.size(); i++) {
    if (tdims[i] != 1 && tdims[i] != idims[i]) {
      this->template logerror<std::invalid_argument>(
          "The time array ({}) has invalid dimension along axis {}. Expected a "
          "length of {} or 1 (for broadcasting), but found {}.",
          vecString(tdims), i, idims[i], tdims[i]);
    }
  }

  // Determine whether to compute in-place or out-of-place.
  // In-place requires T_in == T_out and an explicit flag.
  odims = idims;  // Output will have the same dimensions as the input
  inplace = false;
  // Allow optional in-place demodulation only if T_in == T_out
  if constexpr (std::is_same_v<T_in, T_out>) {
    inplace = performInPlace;
    if (inplace) this->out = this->in;
  }
  // Otherwise, no in place
  if (!inplace) {
    this->out = std::make_shared<Tensor<T_out>>(
        odims, gpuID, this->label + "->out", this->logName);
  }

  this->loginfo("Initialized demodulation.");
}

template <typename T_in, typename T_out>
Demodulate<T_in, T_out>::~Demodulate() {
  // Nothing to do in destructor. Everything will naturally fall out of scope.
  this->loginfo("Destroyed {}.", this->label);
}

template <typename T_in, typename T_out>
void Demodulate<T_in, T_out>::demodulate() {
  this->logdebug("Executing demodulation.");

  // Represent dimensions using dim3*integer to simplify CUDA kernel calls
  // executed over the first 3 dimensions and loop over the remaining "frames".
  int ndims = idims.size();
  id3.x = (ndims > 0) ? idims[0] : 1;
  id3.y = (ndims > 1) ? idims[1] : 1;
  id3.z = (ndims > 2) ? idims[2] : 1;
  td3.x = (ndims > 0) ? tdims[0] : 1;
  td3.y = (ndims > 1) ? tdims[1] : 1;
  td3.z = (ndims > 2) ? tdims[2] : 1;
  od3 = id3;  // Output should have the same dimensions as the input
  T_in *idata = this->in->data();
  float *tdata = times.data();
  T_out *odata = this->out->data();
  size_t ipitch = this->in->getPitch();
  size_t tpitch = times.getPitch();
  size_t opitch = this->out->getPitch();
  size_t istride = ipitch * id3.y * id3.z;
  size_t tstride = tpitch * td3.y * td3.z;
  size_t ostride = opitch * id3.y * id3.z;

  dim3 B(256, 1, 1);  // Heuristically, will almost always demodulate axis=0
  dim3 G((id3.x - 1) / B.x + 1, (id3.y - 1) / B.y + 1, (id3.z - 1) / B.z + 1);

  std::vector<size_t> ifdims, tfdims, ofdims;
  ifdims.push_back(1);
  tfdims.push_back(1);
  ofdims.push_back(1);
  for (int d = 3; d < ndims; d++) {
    ifdims.push_back(idims[d]);
    tfdims.push_back(tdims[d]);
    ofdims.push_back(odims[d]);
  }
  for (int d = 0; d < ofdims.size(); d++) {
    for (int i = 0; i < ofdims[d]; i++) {
      DemodulateKernels::demod<<<G, B, 0, this->stream>>>(
          idata, id3, ipitch, tdata, td3, tpitch, odata, od3, opitch, fdemod);

      // Advance pointers if non-singleton; otherwise, broadcast
      if (ifdims[d] > 1) idata += istride;
      if (tfdims[d] > 1) tdata += tstride;
      if (ofdims[d] > 1) odata += ostride;
    }
  }
}

template <typename T_in, typename T_out>
__global__ void DemodulateKernels::demod(T_in *idata, dim3 idims, int ipitch,
                                         float *tdata, dim3 tdims, int tpitch,
                                         T_out *odata, dim3 odims, int opitch,
                                         float fd) {
  // Always sum over z
  int ox = threadIdx.x + blockIdx.x * blockDim.x;
  int oy = threadIdx.y + blockIdx.y * blockDim.y;
  int oz = threadIdx.z + blockIdx.z * blockDim.z;
  if (ox < odims.x && oy < odims.y && oz < odims.z) {
    // Allow for broadcasting over the time Tensor
    int tx = tdims.x == 1 ? 0 : ox;  // Broadcast if tdims.x == 1
    int ty = tdims.y == 1 ? 0 : oy;  // Broadcast if tdims.y == 1
    int tz = tdims.z == 1 ? 0 : oz;  // Broadcast if tdims.z == 1
    // Compute output index, taking pitch into account
    int iidx = ox + ipitch * (oy + idims.y * oz);  // Input index
    int tidx = tx + tpitch * (ty + tdims.y * tz);  // Time index
    int oidx = ox + opitch * (oy + odims.y * oz);  // Output index

    // Load the input as a cuda::std::complex float
    cuda::std::complex<float> input;
    if constexpr (is_complex<T_in>())
      input = cuda::std::complex<float>(idata[iidx].real(), idata[iidx].imag());
    else
      input = cuda::std::complex<float>(idata[iidx]);
    // Multiply by the complex phasor to demodulate the input
    odata[oidx] = T_out(input * expj2pi(-fd * tdata[tidx]));
  }
}

// Explicit template specialization instantiation for faster compiling
template class Demodulate<cuda::std::complex<short>, cuda::std::complex<short>>;
template class Demodulate<cuda::std::complex<short>, cuda::std::complex<int>>;
template class Demodulate<cuda::std::complex<short>, cuda::std::complex<float>>;
template class Demodulate<cuda::std::complex<int>, cuda::std::complex<int>>;
template class Demodulate<cuda::std::complex<int>, cuda::std::complex<float>>;
template class Demodulate<cuda::std::complex<float>, cuda::std::complex<float>>;
template class Demodulate<short, cuda::std::complex<short>>;
template class Demodulate<short, cuda::std::complex<int>>;
template class Demodulate<short, cuda::std::complex<float>>;
template class Demodulate<int, cuda::std::complex<int>>;
template class Demodulate<int, cuda::std::complex<float>>;
template class Demodulate<float, cuda::std::complex<float>>;

}  // namespace rtbf

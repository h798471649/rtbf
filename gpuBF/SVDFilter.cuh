/**
 @file gpuBF/SVDFilter.cuh
 @author Dongwoon Hyun (dongwoon.hyun@stanford.edu)
 @date 2022-08-12

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef SVDFILTER_CUH_
#define SVDFILTER_CUH_

#include <cublas_v2.h>
#include <cusolverDn.h>

#include "Operator.cuh"

namespace rtbf {

/** @brief Class to apply a spatiotemporal clutter filter to data.
 */
template <typename T_in, typename T_out>
class SVDFilter : public Operator<Tensor<T_in>, Tensor<T_out>> {
 protected:
  // CUDA objects
  int gpuID;
  cusolverDnHandle_t handle;  ///< cuSolverDn handle
  cusolverDnParams_t params;  ///< SVDR parameters
  cublasHandle_t bhandle;     ///< CUBLAS handle
  size_t npixels;             ///< Number of pixels
  size_t nframes;             ///< Number of "slow time" samples
  size_t rank;                ///< Rank of SVD filtering
  bool inplace;               ///< Whether to compute in place
  Tensor<float> W;            ///< Weights to apply to singular values
  Tensor<float> S;            ///< Left singular vector
  Tensor<T_in> U;             ///< Left singular vector
  Tensor<T_in> V;             ///< Right singular vector
  // For sanity, also name the variables according to cusolver's names
  int64_t m, n, lda, ldu, ldv;
  static const cusolverEigMode_t jobz = CUSOLVER_EIG_MODE_VECTOR;
  static const int econ = 1;
  static const cudaDataType computeType =
      is_complex<T_in>() ? CUDA_C_32F : CUDA_R_32F;
  static const cudaDataType typeA = computeType;
  static const cudaDataType typeS = CUDA_R_32F;
  static const cudaDataType typeU = computeType;
  static const cudaDataType typeV = computeType;
  T_in *d_A, *d_U, *d_V;
  float *d_S, *d_W;
  size_t d_lwork, h_lwork;
  void *d_work, *h_work;
  int *d_info, info;
  Tensor<T_in> dw, hw;
  Tensor<int> di;
  double h_err_sigma;

 public:
  /// @brief Constructor
  SVDFilter(std::shared_ptr<Tensor<T_in>> input, const Tensor<float> *h_weights,
            bool computeInPlace = false, cudaStream_t cudaStream = 0,
            std ::string moniker = "SVDFilter", std::string loggerName = "");
  virtual ~SVDFilter();

  /// @brief Apply SVD filter
  void filter();
};

namespace kernels::SVDFilter {
/// @cond KERNELS
/**	@addtogroup SVDFilter Kernels
        @{
*/
/** @brief Multiply weights and singular values with the V matrix
 @param S Pointer to singular value vector
 @param W Pointer to singular value weights
 @param V Pointer to the output image
 @param n Number of columns of V
 @param k Rank of SVD approximation (if any)
*/
template <typename T_in>
__global__ void applyWeights(float *S, float *W, T_in *V, int n, int k);

/** @}*/
/// @endcond

}  // namespace kernels::SVDFilter

// Add template type traits for SFINAE
template <typename>
struct is_SVDFilter : std::false_type {};
template <typename T_in, typename T_out>
struct is_SVDFilter<SVDFilter<T_in, T_out>> : std::true_type {};

}  // namespace rtbf

#endif /* SVDFILTER_CUH_ */
